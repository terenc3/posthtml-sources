# PostHTML Sources Plugin
> Replace links to sources with complied versions

## Examples
```html
<link href="missing-poster.scss" rel="stylesheet" crossorigin="anonymous" />
<script src="missing-poster.ts" crossorigin="anonymous"></script>
```

```html
<link href="missing-poster.min.css" rel="stylesheet" crossorigin="anonymous" />
<script src="missing-poster.min.js" crossorigin="anonymous"></script>
```