module.exports = function(options) {
    options = options || {};
    options.root = options.root || '.';

    /**
     * Insert .min. into filename
     *
     * @param {string} file
     * @returns {string}
     */
    function insertMin(file) {
        const parts = file.split('.')
        return [
            ...parts.slice(0, -1),
            'min',
            ...parts.slice(-1)
        ].join('.')
    }

    /**
     * Replace file suffix
     *
     * @param {string} file
     * @param {string} suffix
     * @returns {string}
     */
    function replaceSuffix(file, suffix) {
        const parts = file.split('.')
        return [
            ...parts.slice(0, -1),
            suffix
        ].join('.')
    }

    /**
     * Convert js and css sources
     *
     * @param {*} tree
     * @returns {*}
     */
    function posthtmlSources(tree) {
        tree.match([
            { tag: 'link', attrs: { rel: 'stylesheet'} },
            { tag: 'script' }
        ], function(node) {
            switch (node.tag) {
                case 'link':
                    if(node.attrs.href.indexOf("http") === 0 ) {
                        return node
                    }
                    node.attrs.href = replaceSuffix(node.attrs.href, 'css')
                    node.attrs.href = insertMin(node.attrs.href)
                    break
                case 'script':
                    if(node.attrs.src.indexOf("http") === 0 ) {
                        return node
                    }
                    node.attrs.src = replaceSuffix(node.attrs.src, 'js')
                    node.attrs.src = insertMin(node.attrs.src)
                    break
            }
            return node
        });

        return tree;
    }

    return posthtmlSources
}